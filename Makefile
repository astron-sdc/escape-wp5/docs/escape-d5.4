DOCHANDLE=ESCAPE-D5.4
export TEXMFHOME ?= astron-texmf/texmf

$(DOCHANDLE).pdf: main.tex meta.tex changes.tex contents.tex
	xelatex -jobname=$(subst .,_,$(DOCHANDLE)) main.tex
	makeglossaries $(subst .,_,$(DOCHANDLE))
	biber $(subst .,_,$(DOCHANDLE))
	xelatex -jobname=$(subst .,_,$(DOCHANDLE)) main.tex
	xelatex -jobname=$(subst .,_,$(DOCHANDLE)) main.tex

contents.tex: contents
	ls contents/[[:digit:]]-*tex | sed -e's/\(.*\)$$/\\include{\1}/' > $@

include astron-texmf/make/vcs-meta.make
include astron-texmf/make/changes.make
