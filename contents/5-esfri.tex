\section{ESFRI Case Studies}
\label{sec:esfri}

The goal of the \gls{ESAP} development effort has been to deliver a system that is of relevance to the various \glspl{ESFRI} that are engaged with the \gls{ESCAPE} project.
As such, even more than the requirements discussed in \cref{sec:requirements}, the true measure of success of the project is the extent to which it meets \gls{ESFRI} needs.
In this section, we highlight the ways in which two of the \glspl{ESFRI} most directly involved in the \gls{ESAP} development effort have driven the direction of the project and may seek to capitalize on \gls{ESAP} development in the future.

\subsection{The \Acrlong{CTAO}}

\subsubsection{Project Overview}

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{images/ctao-telescopes.png}
\caption[Artists impression of the CTA Observatory]{Artists' impressions of the \Acrshort{CTAO} Northern Array (upper illustration; credit: Grabriel Pérez Díaz, IAC) and CTA Southern Array (bottom illustration; credit: Gabriel Pérez Díaz, IAC / Marc-André Besel, CTAO).}
\label{fig:ctao:telescopes}
\end{figure}

The \gls{CTAO} \autocite{doi:10.1142/10986} will be the world's first ground-based gamma-ray observatory, and the world's largest and most sensitive instrument for gamma-ray astronomy at very high energies.
It is currently under construction at two sites --- the \gls{CTAO} Northern Array in La Palma (Spain), and the Southern Array in the Atacama Desert (Chile) --- thereby providing access to both the northern and southern skies.
\gls{CTAO} will consist of three classes of telescopes, covering a broad energy range from 20\,GeV to 300\,TeV the Large-Sized Telescope (LST), the Medium-Sized Telescope (MST) and the Small-Sized Telescope (SST).
During the Construction Phase, the plan for CTAO Northern Array includes 4 LSTs and 9 MSTs, whilst the CTAO Southern Array will feature 14 MSTs and 37 SSTs.

\gls{CTAO} is expected to drive a five- to ten-fold increase in the number of known astronomical gamma-ray sources, potentially detecting as many as 1,000 new previously-unknown objects.
It is particularly suited to observing short (less than a minute) timescale phenomena, making it a key instrument in the future multi-wavelength and multi-messenger infrastructure of time domain astronomy.

The primary scientific drivers of \gls{CTAO} are:

\begin{itemize}

\item{Understanding the origin and role of relativistic cosmic particles;}
\item{Probing extreme environments;}
\item{Exploring frontiers in physics.}

\end{itemize}

For more details on \gls{CTAO} and its involvement with \gls{ESCAPE}, refer to the project's website.\footnote{\url{https://projectescape.eu/science-projects/cherenkov-telescope-array-observatory}}

\subsubsection{Data Infrastructure}

\gls{CTAO} is committed to providing easy access to \gls{CTAO} data products for scientists worldwide, together with high-quality scientific software to analyze the data.
Multi-messenger and multi-wavelength physics are fundamental to the project goals, so it follows that interoperable data and common access mechanisms across different wavelengths, experiments, and observatories are paramount.
\gls{CTAO} will provide support for open science and \gls{FAIR} data management principles.

Specifically, \gls{CTAO} looks to \gls{ESAP} as the potential basis for a science portal providing access to products and services for its user community.

\subsubsection{\glsentrytext{ESAP} Success Stories}

\paragraph{Modularity}

The \gls{CTAO} team identify \gls{ESAP}'s modular and extensible nature --- described in \cref{sec:vision:extensibility} --- as important to their needs: it makes it possible to extend the platform to add new services and archives by building on \gls{REST} interfaces.
This helps enable the identified \gls{CTAO} goals of common access mechanisms.
The project has already put this into practice, for example by linking \pgls{ESAP} with the \gls{OSSR}, as described in \cref{sec:delivered:ida}.

\paragraph{Analysis Workflows}

The \gls{CTAO} Science Portal will offer data and services to its users and staff, with the aim of enabling user interaction with data products and cross-collaboration and joint analysis of data products.
These workflows will build upon new and existing tools, including:

\begin{description}
\item[Gammapy] \autocite{gammapy:2017}, an open-source Python package for gamma-ray astromomy that will form the basis of the \gls{CTAO} science analysis tools; and
\item[AGNpy] \autocite{2022A&A...660A..18N}, a software package developed by high-energy astrophysicists to compute of the photon spectra produced by Active Galactic Nuclei.
\end{description}

Both of these packages have been incorporated in the \gls{ESCAPE} \gls{OSSR}, and are therefore available for use within the \gls{ESAP} environment.

\paragraph{Batch Processing}

\gls{CTAO} operations will require petabyte-scale data management systems.
This will be required for observatory management.
Typically, it will consist of automatic batch data processing tasks which may be distributed over multiple data centres.

The \gls{CTAO} team has been deeply involved in the development of asynchronous (\cref{sec:delivered:gateway}) and batch processing (\cref{sec:delivered:batch}) capabilities within the \gls{ESAP} development effort.
The \gls{ESAP} batch system has therefore been designed to address the use cases expressed by \gls{CTAO}.

\subsubsection{\glsentrytext{CTAO} Science Data Challenge}

\gls{CTAO} anticipates running a \emph{science data challenge} as a way to engage the scientific community with the Observatory's development effort.
Their team is currently exploring the potential for \gls{ESAP} to play a role in such a challenge.
In particular, in support of this, they have developed a way of deploying \gls{ESAP} within a future \gls{CTAO} data centre using the Kubernetes\footnote{\url{https://kubernetes.io}} container orchestration system.
Further, they have developed a series of exploratory customizations of the \gls{ESAP} core, providing:

\begin{itemize}

\item{The capability to use GitHub\footnote{\url{https://github.com}}-based user authentication;}
\item{Additional batch and interactive workflows;}
\item{Advanced workflow search options and improved use of metadata.}

\end{itemize}

Some of these changes have already been fed back to the \gls{ESAP} core distribution.

As part of this work, the \gls{CTAO} team identified the lack of persistent analysis environments within \gls{ESAP} as a significant challenge.
This is a potential topic for future development, which is discussed in \cref{sec:future:persistent}.

\subsubsection{Conclusions}

\gls{CTAO} has been deeply involved in the development of \gls{ESAP} to date, contributing both analysis workflows and core functionality to the project.
They regard \gls{ESAP} as an excellent toolbox for the construction of science platforms, and have successfully been able to deploy and customize it to their needs.
The \gls{CTAO} team has expressed an interest in continuing to collaborate on \gls{ESAP} development beyond the funded duration of the \gls{ESCAPE} project.

\subsection{The SKA Observatory}

\subsubsection{Project Overview}

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{images/ska-night.jpg}
\caption[Artists impression of the SKA telescopes]{Artists' impressions of the SKA telescopes: mid-frequency dishes at left, low frequency stations at right (credit: SKAO).}
\label{fig:skao:telescopes}
\end{figure}

The \gls{SKAO} \autocite{2020PASA...37....2W} is an ongoing effort to build the world's largest radio astronomy observatory.
It consists of a pair of radio interferometers, currently under construction in Australia and South Africa.
The Australian site will house 131,072 antennas sensitive in the range 30--350\,Mhz, while the South African site will consist of 197 dishes covering 0.35--15\,GHz; both systems are illustrated in \cref{fig:skao:telescopes}.
Test systems are already taking data, and the facility is expected to be operational by the end of this decade.

The \gls{SKAO} is designed to conduct transformational science, breaking new ground in astronomical observations.
The observatory has been designed to address a wide range of key science goals, from testing Einstein’s theory of relativity, through understanding the nature of dark energy and cosmic magnetism, to investigating the origins of life itself.

The observatory is being designed to operate for fifty years, so a flexible and extensible system is essential.

For more details on \gls{SKAO} and its involvement with \gls{ESCAPE}, refer to the project's website.\footnote{\url{https://projectescape.eu/science-projects/square-kilometre-array}}

\subsubsection{Data Infrastructure}

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{images/ska-srcs.png}
\caption{Data flow from the SKA Observatory to the Regional Centre Network.}
\label{fig:skao:srcs}
\end{figure}

The SKA Observatory will generate around 700\,PB per year of science-ready data products.
However, the Observatory itself will not make these products directly available to the scientific community.
Rather, they will provide data products to a world-wide distributed network of ``Regional Centres'', as shown in \cref{fig:skao:srcs}.
The members of this Regional Centre Network are then collectively responsible for:

\begin{itemize}

\item{Data logistics, including making data available to end users;}
\item{Data processing, including providing computational and storage resources and appropriate software environments to enable end users to visualize and interact with the data and produce additional data products;}
\item{Data archiving and curation, including data discovery and re-use;}
\item{User support.}

\end{itemize}

The development of this Regional Centre Network is now in its prototyping phase.
As part of this, The Regional Centre development effort is now evaluating a number of potential technologies --- \gls{ESAP} among them --- for use within the SKA Science Platform \autocite{2023:SkaPlatformVision}.

\subsubsection{\glsentrytext{ESAP} Success Stories}

\paragraph{Precursor Data Archives}

Data from SKA precursor instruments has been made available through the \gls{ESAP} interfaces described in \cref{sec:delivered:data}.
This informs decisions about SKA archive structure and data delivery mechanisms.

\paragraph{Jupyter Deployment}

The Jupyter system deployed on the \gls{STFC} cloud has been integrated with the \gls{ESAP} analysis system described in \cref{sec:delivered:ida}.
This includes integration with Binder and authentication and authorization systems.
Further, the system has been extended to support persistent data storage volumes, and to experiment with the Dask parallel computing system.\footnote{\url{https://www.dask.org}}

\paragraph{Data Lake integration}

Although the design is yet to be finalized, a Rucio-based data management system \autocite{rucio:2019}, similar to the \gls{ESCAPE} Data Lake, is under consideration for use within the Regional Centre network.
The \gls{SKAO} team has had a leading role in the integration of the \gls{DLaaS} functionality with \gls{ESAP} with a view to providing access to Regional Centre data services from within the Science Platform environment.

\subsubsection{Conclusions}

The functionality developed within the \gls{ESAP} is directly relevant to the SKA Regional Centre Network effort, although some adaptation and scaling will be required to address the massive data volumes and distributed nature of the systems involved.
The Regional Centre Network is evaluating \gls{ESAP} among other technologies for use in their future development effort.
We have maintained a close relationship between the projects, and believe that \gls{ESAP} offers a compelling value proposition.
However, it is clear that --- whether or not the \gls{ESAP} codebase is directly used in Regional Centre development --- the concepts and techniques pioneered within \gls{ESAP} will have a seminal role within the SKA ecosystem.
