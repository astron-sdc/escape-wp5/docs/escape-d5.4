\section{Future Prospects}
\label{sec:future}

In this section, we summarize prospects for future development of \pgls{ESAP}, beyond the funded scope of the \gls{ESCAPE} Horizon 2020 project.
Broadly, we split this topic into two parts.
First, we address the question of governance and the user community: outside the strictures of \gls{ESCAPE}, who will use \gls{ESAP}?
Who will contribute to its development?
How will that work be organized?
Secondly, we discuss technical aspects of \gls{ESAP} development: what work remains to fully satisfy the vision expressed in \cref{sec:vision}?
What opportunities are there to go beyond that vision?

\subsection{Governance and the User Community}
\label{sec:future:gov}

\subsubsection{\glsentrytext{ESFRI} and Community Engagement}
\label{sec:future:gov:esfri}

The engagement of research infrastructures and other facilities with \gls{ESAP} can take one of two forms.

In the first, major infrastructures --- such as \gls{SKAO} and \gls{CTAO}, as described in \cref{sec:esfri} --- see \gls{ESAP} as a toolkit or library of potentially reusable components.
For example, the distributed and extraordinarily data-intensive nature of the SKA Regional Centres Network means that \gls{ESAP} cannot simply be deployed in its current form: that environment is more complex and specialized than even a tool as generic as \gls{ESAP} can address without modification.
However, it is likely that the Regional Centres effort will build on existing libraries and toolkits --- like \pgls{ESAP} --- to realise its goals (although we note that multiple technologies are under consideration, and the use of \pgls{ESAP}-derived code is not certain).
A similar story applies to other major research infrastructures, certainly including \gls{CTAO}, but also perhaps \gls{ESO}, \gls{JIVE}, and other research infrastructures both within \gls{ESCAPE} and across the wider scientific ecosystem.

A second community of potential users are those research infrastructures which have less exacting needs, and instead are looking for a “plug and play” solution that they can use to rapidly provide functionality to their user community: rather than seeing \gls{ESAP} as components upon which they might build, they look to \gls{ESAP} to provide a ready-to-use solution that can be deployed with minimal modification.
This use case adheres closely to that expressed in \cref{sec:vision}.
However, it is also resource intensive for \gls{ESAP} as a project: while users in the first category may be satisfied with usable components and interesting proofs-of-concept upon which they can build, this second category requires robust, production-grade components, with a mature test suite and extensive documentation.
They are also more risk averse: while the likes of \gls{SKAO} and \gls{CTAO} are able to invest resources in developing \gls{ESAP} itself, other infrastructures will be reluctant to engage until a polished product is available.

\subsubsection{The ESAP Open Source Project}
\label{sec:future:gov:osp}

Within the context of the \gls{ESCAPE} project, \pgls{ESAP} is provided as a mature and well-documented product.
However, maintaining that beyond the scope of \gls{ESCAPE} will take resources and organization.
To address this challenge, we propose to continue development in the form of the \emph{ESAP Open Source Project}.
Specifically, this will be an effort to maintain and develop the core \pgls{ESAP} functionality as community of users.
The project will:

\begin{itemize}

  \item{Maintain the current \pgls{ESAP} code repositories (\cref{sec:access}), or arrange for new hosting when necessary;}
  \item{Continue development and maintenance of core \pgls{ESAP} functionality on a volunteer basis, following the model of other major open source community projects (e.g. Astropy \autocite{2022ApJ...935..167A});}
  \item{Work with major third-party efforts, e.g. the SKA Regional Centre Network and \gls{CTAO}, to incorporate their development into the \pgls{ESAP} core where appropriate;}
  \item{Provide a forum for technical decision making including all members of the \pgls{ESAP} user community, including periodic workshops and meetings.}

\end{itemize}

The \pgls{ESAP} Open Source Project will work closely as part of the future \gls{ESCAPE} Collaboration, and will work with other Collaboration members to ensure that \pgls{ESAP} remains tightly integrated with \gls{ESCAPE} infrastructure and with \gls{EOSC}, and to seek appropriate opportunities to fund ongoing work.

\subsection{Technical Directions}

\gls{ESAP} as delivered presents numerous opportunities for future development to fully realise, and to go beyond, the vision expressed in \cref{sec:vision}.
It is impossible to enumerate all of them here; indeed, to attempt to do so would be undesirable, as it is necessary and appropriate for the directions of development to continue to evolve as requirements change and new opportunities emerge.
Here, however, we suggest a number of goals which follow logically from the work presented in this document, and which describe a compelling case for the future.

\subsubsection{More Flexible Interactive Analysis Systems}
\label{sec:future:ida}

As described in \cref{sec:delivered:ida}, currently interactive analysis in the \gls{ESAP} environment is based around Jupyter notebooks.
While notebooks are currently popular and widespread, other tools are available and may be more appropriate in some circumstances.
This can be conveniently achieved by including generic capabilities for executing containerized analysis tools.
This would open a wider selection of software from the \gls{OSSR} for use through \gls{ESAP}, including making it possible to run graphical “desktop” applications.

Within the context of \gls{ESCAPE} \gls{WP}5 substantial effort has been made in exploring this model using the Rosetta platform \autocite{2022A&C....4100648R}; a logical next step would be to apply the lessons learned and technologies developed there within \gls{ESAP} itself.

\subsubsection{Advanced Batch Processing}
\label{sec:future:batch}

As described in \cref{sec:delivered:batch}, \pgls{ESAP} provides support for starting and interacting with specific batch jobs on DIRAC systems.
There are numerous improvements that could be made here, including:

\begin{itemize}

  \item{Expansion to operate with systems other than DIRAC;}
  \item{A wider range of batch workflows, including interaction with the \gls{OSSR};}
  \item{Making it possible for the user to define and execute their own workflows from within \pgls{ESAP}.}

\end{itemize}

Further, we note that the DIRAC system itself is under active development and there will soon be opportunities for \gls{ESAP} to take advantage of its new capabilities including token-based authorization and a \gls{REST} \gls{API}.

\subsubsection{Advanced Metadata for Job and Infrastructure Selection}
\label{sec:future:metadata}

As described in \cref{sec:delivered:dm,sec:delivered:data,sec:delivered:ida,sec:delivered:batch}, \pgls{ESAP} currently makes it possible for the user to discover data products, to locate software in the \gls{OSSR}, and to bring the two together on a variety of different computing infrastructures.
However, the mechanisms for doing this are relatively inflexible, and rely on substantial user interaction.
A future system could build on richer metadata describing the data, the software, and the computing systems to help the user by suggesting tools or workflows that are relevant to the data they have selected (or vice versa), and by intelligently mapping them to infrastructure based on availability and capability of the computing system and locality of the data.

Some work has already been undertaken in this direction in the context of \gls{ESCAPE} \gls{WP}5 and the \gls{IVOA}, resulting in an \gls{IVOA} note \autocite{2021ivoa.rept.ExecPlanner}.

\subsubsection{Data Publication and Persistent Identifiers}
\label{sec:future:pid}

As described in \cref{sec:delivered:ida}, \gls{ESAP} currently makes it possible for users to save the results of their analysis to the \gls{ESCAPE} Data Lake.
However, to fulfil the vision expressed in \cref{sec:vision:capabilities:provenance} and requirement R-8 (\cref{sec:requirements}), a more advanced data publishing system would be appropriate: \gls{ESAP} should provide a standardized way for scientists to store their workflows and output data products to persistent archival storage, including issuing \glspl{PID} to describe their work.

Work towards this system has been undertaken and deployed at JIV-ERIC in the context of \gls{ESCAPE}, but it has not been integrated with the \gls{ESAP} core.

\subsubsection{Periodic Execution}
\label{sec:future:periodic}

Requirement R-13 (\cref{sec:requirements}) calls for a system whereby users can schedule tasks to repeat at regular intervals.
This might reasonably be extended to include a generic task scheduler interface.
Work on this functionality has not been undertaken in the context of \pgls{ESCAPE}, but it would form an interesting extension to current \gls{ESAP} functionality.

\subsubsection{Persistent, Shareable Analysis Environments}
\label{sec:future:persistent}

The Jupyter environments spawned by \pgls{ESAP} are ephemeral: they exist only for as long as the user's session.
In practice, this limits their usefulness: preferable would be for users to have access to a persistent workspace that they can return to repeatedly as they carry out their analysis over days or weeks with their data, software, and other artefacts stored for them.
The definition of this workspace, and the artefacts contained within it, could be closely related to the data publication system described above (\cref{sec:future:pid}).
An extension would be to make the workspace shareable, so that multiple users can work together on a common project.

\gls{CTAO} and \gls{ESO} have both identified this as a key use case, so it should be a high priority for future development.

\subsubsection{Production-Grade Managed Database}
\label{sec:future:db}

The Managed Database functionality described in \cref{sec:vision:capabilities:db} has been prototyped (\cref{sec:delivered:db}) but is not fully integrated with \pgls{ESAP} and is not ready for production use.
Future development should make it a first class part of the \gls{ESAP} system.

\subsubsection{Federation}
\label{sec:future:fed}

The vision expressed for \pgls{ESAP} in \cref{sec:vision} describes each instance as a fundamentally independent system: no direct communication between individual instances of \pgls{ESAP} is anticipated.
An ambitious future vision would be to remove this limitation: to build a network of \gls{ESAP} systems, each of which can (subject to appropriate authorization) share resources and capabilities with its peers.
Users can build on this system to perform truly multi-disciplinary analyses, while the design of the “network of \glspl{ESAP}” might begin to resemble the SKA Regional Centre network.

Going further, by building such a network on open standards and documented interfaces (for example, by building on the \gls{IVOA} Execution Planner described in \cref{sec:future:metadata}), participation in such a network would not be limited to \gls{ESAP} instances; rather, the network could be open to a wide range of different service types and implementations, truly enabling a virtual research environment for open science.
