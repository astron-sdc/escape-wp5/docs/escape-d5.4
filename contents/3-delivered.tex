\section{Delivered Capabilities}
\label{sec:delivered}

This section provides an overview of the \pgls{ESAP} capabilities delivered within the scope of the \gls{ESCAPE} project, and describes how they relate to the vision described in \cref{sec:vision}.
The delivered version of \gls{ESAP} does not meet the full scope of the vision: this is expected, and reflects the wide scope of the problem domain that \gls{ESAP} addresses and the potential for future development (discussed in \cref{sec:future}).
However, these delivered capabilities do provide a flexible and compelling system that \glspl{ESFRI} and other projects can build upon, as discussed in \cref{sec:esfri}.

Most of the capabilities described here are available through a test deployment of \pgls{ESAP} at ASTRON.
This system may be accessed at \url{https://sdc-dev.astron.nl/esap-gui/}.
Note that this system is provided without support to facilitate development and demonstration; the service is not expected to be reliable, nor to be available indefinitely.

\subsection{User Interface}
\label{sec:delivered:ui}

The front page of the \pgls{ESAP} test deployment at ASTRON is shown in \cref{fig:esap:front}.
It presents an attractive and usable web-based interface that enables the user to rapidly discover which services are configured in this \pgls{ESAP} instance (in the top bar; here, Archives, Multi Query, Interactive Analysis, Batch Analysis, Asynchronous Jobs and IVOA-SAMP).
The rest of the interface highlights the available archives: in this case, corresponding to Apertif \autocite{2018wtfa.confE..16O}, the ASTRON \gls{VO} system\footnote{\url{https://vo.astron.nl/}}, the Zooniverse citizen science system\footnote{\url{https://www.zooniverse.org/}}, and the general \gls{IVOA} Virtual Observatory \autocite{2010ivoa.rept.1123A}.

The interface is a cross-platform web application implemented in React\footnote{\url{https://reactjs.org/}}, which communicates with the API Gateway (\cref{sec:delivered:gateway}) over a \gls{REST} interface.

\begin{figure}
\centering
\includegraphics[width=0.66\textwidth]{images/esap-front.png}
\caption{The front page of the \glsentrytext{ESAP} test deployment at ASTRON.}
\label{fig:esap:front}
\end{figure}

\subsection{API Gateway}
\label{sec:delivered:gateway}

As discussed in \cref{sec:vision:capabilities:ui}, the “API Gateway” provides back-end capabilities for the \pgls{ESAP} system.
The API Gateway is written in Python, using Django\footnote{\url{https://www.djangoproject.com/}} and its companion \gls{REST} framework\footnote{\url{https://www.django-rest-framework.org/}}.

The API Gateway provides a rich, plugin-based system for adding new service integrations to \gls{ESAP}.
It also provides an asynchronous job control system, shown in \cref{fig:esap:async}, which is used to manage long-running tasks like some queries (\cref{sec:delivered:data}) and batch processing jobs (\cref{sec:delivered:batch}).

In principle, it would be possible for alternative or specialist user interfaces to communicate with the API Gateway using the same interface; as of now, however, the authors are not aware of any other \pgls{ESAP} interfaces.

\begin{figure}
\centering
\includegraphics[width=0.66\textwidth]{images/esap-async.png}
\caption{Asynchronous job management in \glsentrytext{ESAP}.}
\label{fig:esap:async}
\end{figure}


\subsection{Authentication and Authorization}
\label{sec:delivered:auth}

\begin{figure}
\centering
\includegraphics[width=0.66\textwidth]{images/esap-auth.png}
\caption{Using \glsentrytext{ESCAPE} \glsentrytext{IAM} to authenticate with \glsentrytext{ESAP}.}
\label{fig:esap:auth}
\end{figure}

\pgls{ESAP} is fully integrated with the \gls{ESCAPE} project's \gls{IAM} service.
\cref{fig:esap:auth} shows an example of a user authenticating with the \pgls{ESAP} test system using \gls{ESCAPE} \gls{IAM}.

\subsection{Data Orchestration within \glsentrytext{ESAP}}
\label{sec:delivered:dm}

\begin{figure}
\centering
\includegraphics[width=0.66\textwidth]{images/esap-basket.png}
\caption{The “shopping basket” viewed through the \glsentrytext{ESAP} web interface.}
\label{fig:esap:basket}
\end{figure}

Shopping basket capabilities, as described in \cref{sec:vision:capabilities:orch}, are available in the current version of \gls{ESAP} to users who have authenticated through \gls{ESCAPE} \gls{IAM} (\cref{sec:delivered:auth}).

\cref{fig:esap:basket} shows an example of the shopping basket visualized through the web interface.
Note that the formatting of the contents of the basket varies depending on the source of the data.

\subsection{Data Discovery}
\label{sec:delivered:data}

\begin{figure}
\centering
\begin{subfigure}{0.48\textwidth}
\includegraphics[width=\textwidth]{images/esap-apertif-results.png}
\subcaption{Apertif.}
\label{sub:esap:query:apertif}
\end{subfigure}
\begin{subfigure}{0.48\textwidth}
\includegraphics[width=\textwidth]{images/esap-zooniverse-results.png}
\subcaption{Zooniverse.}
\label{sub:esap:query:zooniverse}
\end{subfigure}
\caption{Query results displayed though \glsentrytext{ESAP}.}
\label{fig:esap:query}
\end{figure}

Plugins which enable \pgls{ESAP} to interface with a variety of archive systems --- including Apertif, ASTRON VO, Zooniverse and the \gls{KIS} Science Data Centre \footnote{\url{https://sdc.leibniz-kis.de/en/}} --- are provided as part of the core \pgls{ESAP} distribution.
Examples are shown in \cref{fig:esap:query}.
Note that the query interface and the form of the results returned adapt appropriately depending on the nature of the archive in question, so that --- for example --- the user is given the opportunity to specify celestial coordinates when querying the Apertif archive, but not when querying the Zooniverse system.

The leftmost columns in the result interface give the user the opportunity to select individual rows from the results to add them to their shopping basket (\cref{sec:delivered:dm}).
In the future (see \cref{sec:future}), we hope to extend \pgls{ESAP} to offer more flexibility here; this might include, for example, bulk addition of many rows to the basket without selecting each individually.

\begin{figure}
\centering
\includegraphics[width=0.66\textwidth]{images/esap-multiquery.png}
\caption{Simultaneously querying multiple archives through ESAP.}
\label{fig:esap:multiquery}
\end{figure}

Normally, archives are queried individually --- that is, the user selects which archive they are interested in and directs the query towards it.
However, \pgls{ESAP} also provides a multi-archive query mode in which the user's query is forwarded to multiple archives simultaneously, with \gls{ESAP} taking care of translating the query into a form which is appropriate to each archive and merging the results to present to the user in a unified form.
This is illustrated in \cref{fig:esap:multiquery}.

By default, queries are made synchronously: that is, when the user submits a query it is immediately forwarded to the appropriate archive (or archives) and the user interface is blocked until results are available.
While this is appropriate for interactive use, it is not suitable for every archive.
In particular, large or heavily loaded archive sites may not process queries immediately or may take a long time to respond.
For example, the \gls{LSST} archive system will group multiple user queries together and periodically execute them at the same time in a “shared scan” \autocite{10.1145/2063348.2063364}.
\pgls{ESAP}'s asynchronous execution system (\cref{sec:delivered:gateway}) makes it possible for queries of this type to be handled as background jobs, with the user returning later to collect their results.

\subsection{\glsentrytext{SAMP}}
\label{sec:delivered:samp}

\pgls{ESAP} incorporates support for \gls{SAMP} using the sampjs library\footnote{\url{https://github.com/astrojs/sampjs}}.
Users can transmit data to the ESAP web interface from other \gls{SAMP}-enabled applications, and from there add it to their shopping basket.
Transmission in the other direction --- from \pgls{ESAP} to other applications --- is not yet available.

\subsection{\glsentrydesc{IDA}}
\label{sec:delivered:ida}

\begin{figure}
\centering
\begin{subfigure}[t]{0.48\textwidth}
\includegraphics[width=\textwidth]{images/esap-workflow-select.png}
\subcaption{Workflow selection.}
\label{sub:esap:ida:basket}
\end{subfigure}
\begin{subfigure}[t]{0.48\textwidth}
\includegraphics[width=\textwidth]{images/esap-analysis.png}
\subcaption{Jupyter notebook.}
\label{sub:esap:ida:notebook}
\end{subfigure}
\caption{The \glsentrytext{ESAP} \glsentrytext{IDA} workflow.}
\label{fig:esap:ida}
\end{figure}

\pgls{ESAP} makes it possible for users to execute interactive analysis jobs --- in the shape of Jupyter notebooks \autocite{jupyter:2016} --- through a three-stage process:

\begin{enumerate}

  \item{The user selects from a curated list of available analysis notebooks (\cref{sub:esap:ida:basket});}
  \item{The user selects from a range of available compute systems which are capable of executing their chosen analysis task;}
  \item{The user is launched into the executable notebook running on the system of their choice (\cref{sub:esap:ida:notebook}).}

\end{enumerate}

The list of available notebooks is drawn from the \gls{ESCAPE} \gls{OSSR}, and it may be augmented by local administrators through \gls{ESAP}'s configuration.
The list is searchable and filterable to help the users identify notebooks that are relevant to their needs.
Although \pgls{ESAP} as delivered does not support other software repositories, it would be a straightforward adaptation to extend this system to search for software available from sources other than the \gls{OSSR}.

The available compute systems are defined in the \pgls{ESAP} configuration.
It is expected that administrators of specific \gls{ESAP} instances will customize the listing to their particular needs.

Launching notebooks is handed by the BinderHub\footnote{\url{https://binderhub.readthedocs.io/}}.
As such, limited computational resources can always be made available using the freely available MyBinder\footnote{\url{https://www.mybinder.org/}} system, should the \gls{ESAP} administrator choose to enable it for their particular instance.

Upon choosing an appropriate workflow and analysis service, the user is redirected to the notebook environment.
In that environment, a Python library --- initially developed specifically to address Zooniverse classification data, but now adapted to a wide range of data types --- makes it possible for them to access their \pgls{ESAP} shopping basket, and hence to download or otherwise manipulate the data that they have selected.
In addition, the \gls{DLaaS} system, developed in conjunction with \gls{ESCAPE} \gls{WP}2 (\Acrshort{DIOS}), provides integration between the interactive analysis environment and bulk storage offered through the \gls{ESCAPE} Data Lake.

Although the current \gls{IDA} system focuses on Jupyter notebooks, we expect to extend this service to address other forms of interaction in future work; refer to \cref{sec:future:ida} for further discussion.

\subsection{Batch Processing}
\label{sec:delivered:batch}

\pgls{ESAP} builds on the asynchronous job management system described in \cref{sec:delivered:gateway} to provide a batch computing environment that is capable of launching jobs to a DIRAC \autocite{dirac:2022} environment, monitoring their progress.

This system currently works with a limited number of pre-defined batch processing workflows.
Future work (\cref{sec:future:batch}) may include extension to a more flexible job definition system and integration with additional batch processing middleware.

\subsection{Managed Database}
\label{sec:delivered:db}

A prototype Managed Database service has been developed within the scope of the \gls{ESCAPE} project and can be deployed alongside the \pgls{ESAP} core.
This service currently serves as a technology demonstrator; it is not fully integrated with the \pgls{ESAP} core, and is not yet ready to be deployed in a production environment.
However, it points towards potential future development directions (\cref{sec:future:db}).
